
function[epsr, depsw] = Eps_DrudeLorentz(w, wp, f, gamma, w0, epsInf)
% EPS_LORENTZDRUDE  return epsilon & d(eps*w)/dw for a multi-oscillator model
%   [epsr, depsw] = Eps_DrudeLorentz(w, wp, f, gamma, w0, epsInf)
%   inputs: a single wp, an array f to multiply wp^2 by, 
%           an array of gamma's, and an array of w0
%           epsInf (optional -- default = 1)
if (nargin > 5)
    epsr = epsInf;
else
    epsr = 1;
end
deps = 0;
for i = 1:length(f)
	epsr = epsr - f(i) * wp^2 ./ (w.^2 - w0(i)^2 + 1i * gamma(i) * w);
    deps = deps + real( f(i) * (2 * w + 1i * gamma(i)) * wp^2 ./ (w.^2 - w0(i)^2 + 1i * gamma(i) * w).^2 ); 
end
depsw = epsr + w .* deps;
end
