
function[epsr, depsw, wp, f, gamma, w0] = Ag_DL_Palik(w)
% AG_DL_PALIK Lorentz-Drude model for Palik Ag
% [eps, depsw, wp, f, gamma, w0] = Ag_DL_Palik(w)
%     NOTE: For lambda > 1240nm may want to use Ag_LD instead
% See also AG_LD

const = MATLAB_CONSTANTS;
epsInf =2.3622;
wp = 8.7376;
g0 = 0.0696;
C1 = 6.2581;
w1 = 3.9957;
g1 = 0.0468;
C2 = 24.0691;
w2 = 4.9844;
g2 = 1.3184;

% 	A = 1/4;
% 	fermiV = 1.39e6; % fermi velocity (m/s)
% 	if(nargin==1)
% 		SAtoVol = 0;
% 	end
% 	g0 = g0 + A * fermiV * SAtoVol * const.wToEV;
%

% w = w * const.wToEV;

f = [1; C1/wp^2; C2/wp^2];
gamma = [g0; 2*g1; 2*g2] / const.wToEV;
w0 = [0; w1; w2] / const.wToEV;

wp = wp / const.wToEV;

[epsr, depsw] = Eps_DrudeLorentz(w, wp, f, gamma, w0, epsInf);

end
