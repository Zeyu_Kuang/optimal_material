% material: Ag
% reference: 
% 1. Johnson and Christy 1972 
% 2. Yang et al. 2015 
% 3. McPeak et al. 2015 
% 4. Babar and Weaver 2015
% 5. ED Palik 1998

% data source: 1-4 from refractiveindex.info. 5 from group bitbucket. 

% material: Au
% reference: 
% 1. Johnson and Christy 1972 
% 2. Olmon et al. 2015 (template-stripped)
% 3. McPeak et al. 2015 
% 4. Babar and Weaver 2015
% 5. Palik 1998

% data source: 1-4 from refractiveindex.info. 5 from group bitbucket.

clc
clear
addpath('func')

%% read Ag
T1 = readtable('data/Johnson_Ag.csv'); 
T2 = readtable('data/Yang_Ag.csv'); 
T3 = readtable('data/McPeak_Ag.csv');  
T4 = readtable('data/Babar_Ag.csv'); 

[Ag_wl1, Ag_FOM1, Ag_eps1] = extract_FOM(T1);
[Ag_wl2, Ag_FOM2, Ag_eps2] = extract_FOM(T2);
[Ag_wl3, Ag_FOM3, Ag_eps3] = extract_FOM(T3);
[Ag_wl4, Ag_FOM4, Ag_eps4] = extract_FOM(T4);
[Ag_wl5, Ag_FOM5, Ag_eps5] = Ag_FOM_Palik();

%% read Au
T1 = readtable('data/Johnson_Au.csv'); 
T2 = readtable('data/Olmon-ts_Au.csv'); 
T3 = readtable('data/McPeak_Au.csv');  
T4 = readtable('data/Babar_Au.csv'); 

[Au_wl1, Au_FOM1, Au_eps1] = extract_FOM(T1);
[Au_wl2, Au_FOM2, Au_eps2] = extract_FOM(T2);
[Au_wl3, Au_FOM3, Au_eps3] = extract_FOM(T3);
[Au_wl4, Au_FOM4, Au_eps4] = extract_FOM(T4);
[Au_wl5, Au_FOM5, Au_eps5] = Au_FOM_Palik();


%% plot
figure()
set(gcf,'position',[100,100,1000,400])
subplot(1,2,1)
hold on
plot(Ag_wl1, Ag_FOM1, '-o','linewidth',1)
plot(Ag_wl2, Ag_FOM2, '-','linewidth',2)
plot(Ag_wl3, Ag_FOM3, '-','linewidth',2)
plot(Ag_wl4, Ag_FOM4, '-','linewidth',2)
plot(Ag_wl5, Ag_FOM5, '-o','linewidth',1)
xlim([0.4,2])
%ylim([1e0,1e4])
xlabel('Wavelength, \lambda (�m)')
ylabel('|\chi|^2/ Im(\chi)')
title('Ag')
legend({'Johnson and Christy', 'Yang et al.', 'McPeak et al.', 'Babar and Weaver', 'Palik'},'location','southeast')
set(gca,'fontsize',16)
set(gca,'YScale','log')
set(gca, 'XTick', 0:0.2:2)
box on


subplot(1,2,2)
hold on
plot(Au_wl1, Au_FOM1, '-o','linewidth',1)
plot(Au_wl2, Au_FOM2, '-','linewidth',2)
plot(Au_wl3, Au_FOM3, '-','linewidth',2)
plot(Au_wl4, Au_FOM4, '-','linewidth',2)
plot(Au_wl5, Au_FOM5, '-o','linewidth',1)
xlim([0.4,2])
%ylim([1e0,1e4])
xlabel('Wavelength, \lambda (�m)')
ylabel('|\chi|^2/ Im(\chi)')
title('Au')
legend({'Johnson and Christy', 'Olmon et al.', 'McPeak et al.', 'Babar and Weaver', 'Palik'},'location','southeast')
set(gca,'fontsize',16)
set(gca,'YScale','log')
set(gca, 'XTick', 0:0.2:2)
box on

%% functions
function [wl, FOM, eps] = extract_FOM(T)
    % input: T - table element 
    % output: wl - wavelength (um)
    %         FOM - figure of merit
    %         eps - electric permittivity
    
    wl = T.wl;
    n = T.n;
    k = T.k;

    eps1 = n.^2 - k.^2;
    eps2 = 2*n.*k;

    eps = eps1 + 1j*eps2;

    chi = eps - 1;
    FOM = abs(chi).^2 ./ imag(chi);
end

function [wl, FOM, eps] = Ag_FOM_Palik()
    Ag = load('data/Ag_Palik.mat');
    wl = Ag.lambda * 1e6; % m -> um
    eps = Ag.eps;
    chi = eps - 1;
    FOM = abs(chi).^2 ./ imag(chi);
end

function [wl, FOM, eps] = Au_FOM_Palik()
    Ag = load('data/Au_Palik.mat');
    wl = Ag.lambda * 1e6; % m -> um
    eps = Ag.eps;
    chi = eps - 1;
    FOM = abs(chi).^2 ./ imag(chi);
end



