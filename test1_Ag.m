% material: Ag
% reference: 
% 1. Johnson and Christy 1972 
% 2. Yang et al. 2015 
% 3. McPeak et al. 2015 
% 4. Babar and Weaver 2015
% 5. ED Palik 1998

% data source: 1-4 from refractiveindex.info. 5 from group bitbucket. 

clc
clear
addpath('func')

%%
T1 = readtable('data/Johnson_Ag.csv'); 
T2 = readtable('data/Yang_Ag.csv'); 
T3 = readtable('data/McPeak_Ag.csv');  
T4 = readtable('data/Babar_Ag.csv'); 

[wl1, FOM1, eps1] = extract_FOM(T1);
[wl2, FOM2, eps2] = extract_FOM(T2);
[wl3, FOM3, eps3] = extract_FOM(T3);
[wl4, FOM4, eps4] = extract_FOM(T4);
[wl5, FOM5, eps5] = Ag_FOM_Palik();

%% plot
figure()
hold on
plot(wl1, FOM1, '-o','linewidth',1)
plot(wl2, FOM2, '-','linewidth',2)
plot(wl3, FOM3, '-','linewidth',2)
plot(wl4, FOM4, '-','linewidth',2)
plot(wl5, FOM5, '-o','linewidth',2)
xlim([0.4,2])
%ylim([1e0,1e5])
xlabel('Wavelength, $\lambda$ ($\mu$m)','interpreter','latex')
ylabel('$|\chi|^2$/ Im$(\chi)$','interpreter','latex')
title('Ag', 'interpreter', 'latex')
legend({'Johnson and Christy', 'Yang et al.', 'McPeak et al.', 'Babar and Weaver', 'Palik'},'location','southeast' ...
    , 'interpreter', 'latex')
set(gca,'fontsize',16)
set(gca,'YScale','log')
set(gca, 'XTick', 0:0.2:2)
set(gca,'TickLabelInterpreter','latex')
box on

%% functions
function [wl, FOM, eps] = extract_FOM(T)
    % input: T - table element 
    % output: wl - wavelength (um)
    %         FOM - figure of merit
    %         eps - electric permittivity
    
    wl = T.wl;
    n = T.n;
    k = T.k;

    eps1 = n.^2 - k.^2;
    eps2 = 2*n.*k;

    eps = eps1 + 1j*eps2;

    chi = eps - 1;
    FOM = abs(chi).^2 ./ imag(chi);
end

function [wl, FOM, eps] = Ag_FOM_Palik()
    Ag = load('data/Ag_Palik.mat');
    wl = Ag.lambda * 1e6; % m -> um
    eps = Ag.eps;
    chi = eps - 1;
    FOM = abs(chi).^2 ./ imag(chi);
end


