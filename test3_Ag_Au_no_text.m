% material: Ag
% reference: 
% 1. Johnson and Christy 1972 
% 2. Yang et al. 2015 
% 3. McPeak et al. 2015 
% 4. Babar and Weaver 2015
% 5. ED Palik 1998
% 6. Ciesielski et al. 2017 (20-nm thin film, Ag/SiO2)

% data source: 1-4,6 from refractiveindex.info. 5 from group bitbucket. 

% material: Au
% reference: 
% 1. Johnson and Christy 1972 
% 2. Olmon et al. 2015 (template-stripped)
% 3. McPeak et al. 2015 
% 4. Babar and Weaver 2015
% 5. Palik 1998
% 6. Yakubovsky et al. 2017: 25-nm film; n,k 0.3-2 �m

% data source: 1-4,6 from refractiveindex.info. 5 from group bitbucket.

clc
clear
addpath('func')

%% read Ag
T1 = readtable('data/Johnson_Ag.csv'); 
T2 = readtable('data/Yang_Ag.csv'); 
T3 = readtable('data/McPeak_Ag.csv');  
T4 = readtable('data/Babar_Ag.csv');  
T6 = readtable('data/Ciesielski_Ag.csv'); 

[Ag_wl1, Ag_FOM1, Ag_eps1] = extract_FOM(T1);
[Ag_wl2, Ag_FOM2, Ag_eps2] = extract_FOM(T2);
[Ag_wl3, Ag_FOM3, Ag_eps3] = extract_FOM(T3);
[Ag_wl4, Ag_FOM4, Ag_eps4] = extract_FOM(T4);
[Ag_wl6, Ag_FOM6, Ag_eps6] = extract_FOM(T6);
[Ag_wl5, Ag_FOM5, Ag_eps5] = Ag_FOM_Palik();

%% read Au
T1 = readtable('data/Johnson_Au.csv'); 
T2 = readtable('data/Olmon-ts_Au.csv'); 
T3 = readtable('data/McPeak_Au.csv');  
T4 = readtable('data/Babar_Au.csv'); 
T6 = readtable('data/Yakubovsky_Au.csv'); 

[Au_wl1, Au_FOM1, Au_eps1] = extract_FOM(T1);
[Au_wl2, Au_FOM2, Au_eps2] = extract_FOM(T2);
[Au_wl3, Au_FOM3, Au_eps3] = extract_FOM(T3);
[Au_wl4, Au_FOM4, Au_eps4] = extract_FOM(T4);
[Au_wl6, Au_FOM6, Au_eps6] = extract_FOM(T6);
[Au_wl5, Au_FOM5, Au_eps5] = Au_FOM_Palik();

%% color
c_blue = [0, 0.4470, 0.7410];
c_oran = [0.8500, 0.3250, 0.0980];
c_yell = [0.9290, 0.6940, 0.1250];
c_purp = [0.4940, 0.1840, 0.5560];
c_gree = [0.4660, 0.6740, 0.1880];
c_grey = [0.25, 0.25, 0.25];

%% plot
figure()
set(gcf,'color','w')
set(gcf,'position',[100,100,1000,400])
subplot(1,2,1)
hold on
plot(Ag_wl1, Ag_FOM1, '-o', 'color', c_blue, 'linewidth',1)
plot(Ag_wl2, Ag_FOM2, '-', 'color', c_oran,'linewidth',2)
plot(Ag_wl3, Ag_FOM3, '-', 'color', c_yell,'linewidth',2)
plot(Ag_wl4, Ag_FOM4, '-', 'color', c_purp,'linewidth',2)
plot(Ag_wl5, Ag_FOM5, '-o', 'color', c_gree,'linewidth',1)
plot(Ag_wl6, Ag_FOM6, '-', 'color', c_grey,'linewidth',2)
xlim([0.4,2])
%ylim([1e0,1e4])
xlabel('Wavelength, \lambda (�m)')
ylabel('|\chi|^2/ Im\chi')
set(gca,'fontsize',16)
set(gca,'YScale','log')
set(gca, 'XTick', 0:0.2:2)
box on

% text
text(0.42,8e3,'(a)', 'color', 'k','fontsize',16)
text(0.57,7.6e3,'Ag', 'color', 'k','fontsize',16)
text(0.45,4e3,'Johnson', 'color', c_blue,'fontsize',16)
text(0.41,1.5e3,'Yang', 'color', c_oran,'fontsize',16)
text(1.5,4e3,'McPeak', 'color', c_yell,'fontsize',16)
text(1.2,7e3,'Babar', 'color', c_purp,'fontsize',16)
text(.8,4e2,'Palik', 'color', c_gree,'fontsize',16)
text(0.45,4e1,'Ciesielski', 'color', c_grey,'fontsize',16)

subplot(1,2,2)
hold on
plot(Au_wl1, Au_FOM1, '-o', 'color', c_blue,'linewidth',1)
plot(Au_wl2, Au_FOM2, '-', 'color', c_oran,'linewidth',2)
plot(Au_wl3, Au_FOM3, '-', 'color', c_yell,'linewidth',2)
plot(Au_wl4, Au_FOM4, '-', 'color', c_purp,'linewidth',2)
plot(Au_wl5, Au_FOM5, '-o', 'color', c_gree,'linewidth',1)
plot(Au_wl6, Au_FOM6, '-', 'color', c_grey,'linewidth',2)
xlim([0.4,2])
%ylim([1e0,1e4])
xlabel('Wavelength, \lambda (�m)')
ylabel('|\chi|^2/ Im\chi')
set(gca,'fontsize',16)
set(gca,'YScale','log')
set(gca, 'XTick', 0:0.2:2)
box on

% text
text(0.42,7.3e3,'(b)', 'color', 'k','fontsize',16)
text(0.57,7e3,'Au', 'color', 'k','fontsize',16)
text(1.1,4e2,'Johnson', 'color', c_blue,'fontsize',16)
text(0.75,1.5e2,'Olmon', 'color', c_oran,'fontsize',16)
text(0.8,3e3,'McPeak', 'color', c_yell,'fontsize',16)
text(1.6,6.5e3,'Babar', 'color', c_purp,'fontsize',16)
text(0.55,6e0,'Palik', 'color', c_gree,'fontsize',16)
text(0.63,4e1,'Yakubovsky', 'color', c_grey,'fontsize',16)

% save
export_fig('test3_Ag_Au_with_text.pdf')

%% functions
function [wl, FOM, eps] = extract_FOM(T)
    % input: T - table element 
    % output: wl - wavelength (um)
    %         FOM - figure of merit
    %         eps - electric permittivity
    
    wl = T.wl;
    n = T.n;
    k = T.k;

    eps1 = n.^2 - k.^2;
    eps2 = 2*n.*k;

    eps = eps1 + 1j*eps2;

    chi = eps - 1;
    FOM = abs(chi).^2 ./ imag(chi);
end

function [wl, FOM, eps] = Ag_FOM_Palik()
    Ag = load('data/Ag_Palik.mat');
    wl = Ag.lambda * 1e6; % m -> um
    eps = Ag.eps;
    chi = eps - 1;
    FOM = abs(chi).^2 ./ imag(chi);
end

function [wl, FOM, eps] = Au_FOM_Palik()
    Ag = load('data/Au_Palik.mat');
    wl = Ag.lambda * 1e6; % m -> um
    eps = Ag.eps;
    chi = eps - 1;
    FOM = abs(chi).^2 ./ imag(chi);
end



